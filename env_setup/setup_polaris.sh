source /lus/grand/projects/datascience/parton/kokkos/kokkos-4.1.00/Kokkos_ARCH_AMPERE80/Release/setup.sh
module load cray-hdf5-parallel
export PEPPER_DATA_PATH=/lus/grand/projects/datascience/parton/pepper_data
export PKG_CONFIG_PATH=/home/parton/git/lhapdf/install/lib/pkgconfig:$PKG_CONFIG_PATH
export FORM_PATH=/home/parton/git/form/install/
export PATH=$FORM_PATH/bin:$PATH
