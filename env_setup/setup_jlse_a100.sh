source /projects/datascience/parton/kokkos/kokkos-4.1.00/Kokkos_ARCH_AMPERE80/Release/setup.sh
source /home/jchilders/git/lhapdf/setup.sh
module load spack openmpi
export PEPPER_DATA_PATH=/projects/AtlasESP/data/blockgen
export PEPPER_CACHE_PATH=/projects/datascience/parton/pepper_cache
export PATH=$PATH:/home/jchilders/git/form/install/bin
module load spack hdf5
