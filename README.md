# Pepper Extras

A place to store expert scripts and other support files.

## Environment Setup
In the [`env_setup` folder](./env_setup/) there are example setup scripts used to run Pepper on various hardware for the performance tests presented in publications.


## Performance Runs
In the [`perf_runs` folder](./perf_runs/) there are helper Python scripts used to run Pepper performance runs.

## Batch Submission Scripts
In the[`batch_submits` folder](./batch_submits/) there are example scripts for submitting jobs on various systems.