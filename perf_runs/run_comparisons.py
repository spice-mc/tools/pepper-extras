#!/gpfs/jlse-fs0/projects/datascience/parton/conda/2022-03/bin/python
import argparse,logging
import subprocess,time,os,glob,sys
import pandas as pd
import numpy as np
import h5py as hp
from typing import List,Tuple,Dict,Any
import socket
hostname = socket.getfqdn()
logger = logging.getLogger(__name__)

base = '/projects/AtlasESP/data/multiprocess' # JLSE
if 'hopper' in hostname:
   base = '/tmp/jchilders' # H100
if 'americas' in hostname:
   base = '/lus/gila/projects/atlas_aesp_CNDA/pepper/2023-10-27'
pepper_data = base + '/pepper_data'
pepper_grids = base + '/pepper_grids'
if 'ftm.alcf' in hostname:
   os.environ["PEPPER_DATA_PATH"] = "/projects/AtlasESP/data/multiprocess/pepper_data"
process_configs ={
   'p p -> t tb': {
      'process_file': pepper_data + '/tt0j.csv',
      'n_batches': 50e3,
      'pepper_cache': pepper_grids + '/tt+0j/pepper_cache',
      'mu2': 'm_t^2',
   },
   'p p -> t tb j': {
      'process_file': pepper_data + '/tt1j.csv',
      'n_batches': 20e3,
      'pepper_cache': pepper_grids + '/tt+1j/pepper_cache',
      'mu2': 'm_t^2',
   },
   'p p -> t tb j j': {
      'process_file': pepper_data + '/tt2j.csv',
      'n_batches': 10e3,
      'pepper_cache': pepper_grids + '/tt+2j/pepper_cache',
      'mu2': 'm_t^2',
   },
   'p p -> t tb j j j': {
      'process_file': pepper_data + '/tt3j.csv',
      'n_batches': 1e3,
      'pepper_cache': pepper_grids + '/tt+3j/pepper_cache',
      'mu2': 'm_t^2',
   },
   'p p -> t tb j j j j': {
      'process_file': pepper_data + '/tt4j.csv',
      'n_batches': 1e3,
      'pepper_cache': pepper_grids + '/tt+4j/pepper_cache',
      'mu2': 'm_t^2',
   },
   'p p -> Z': {
      'process_file': pepper_data + '/z0j.csv',
      'n_batches': 50e3,
      'pepper_cache': pepper_grids + '/Z+0j/pepper_cache',
      'mu2': 'm_Z^2',
   },
   'p p -> Z j': {
      'process_file': pepper_data + '/z1j.csv',
      'n_batches': 20e3,
      'pepper_cache': pepper_grids + '/Z+1j/pepper_cache',
      'mu2': 'm_Z^2',
   },
   'p p -> Z j j': {
      'process_file': pepper_data + '/z2j.csv',
      'n_batches': 10e3,
      'pepper_cache': pepper_grids + '/Z+2j/pepper_cache',
      'mu2': 'm_Z^2',
   },
   'p p -> Z j j j': {
      'process_file': pepper_data + '/z3j.csv',
      'n_batches': 1e3,
      'pepper_cache': pepper_grids + '/Z+3j/pepper_cache',
      'mu2': 'm_Z^2',
   },
   'p p -> Z j j j j': {
      'process_file': pepper_data + '/z4j.csv',
      'n_batches': 1e3,
      'pepper_cache': pepper_grids + '/Z+4j/pepper_cache',
      'mu2': 'm_Z^2',
   },
   'p p -> Z j j j j j': {
      'process_file': pepper_data + '/z5j.csv',
      'n_batches': 1e3,
      'pepper_cache': pepper_grids + '/Z+5j/pepper_cache',
      'mu2': 'm_Z^2',
   }
}


processes = [
   'p p -> t tb',
   'p p -> t tb j',
   'p p -> t tb j j',
   'p p -> t tb j j j',
   'p p -> t tb j j j j',

   # 'p p -> Z',
   # 'p p -> Z j',
   # 'p p -> Z j j',
   # 'p p -> Z j j j',
   # 'p p -> Z j j j j',
   # 'p p -> Z j j j j j',

   # "g g -> t tb",
   # "g g -> t tb g",
   # "g g -> t tb g g",
   # "g g -> t tb g g g",
   # "g g -> t tb g g g g",
   # "g g -> t tb g g g g g",

   # "d u  -> d u",
   # "g g  -> d d*",

   # "d u  -> d u g",
   # "g g  -> g g g",
   # "d u* -> d u* g",

   # "g g  -> g g g g",
   # "d db -> u s ub sb",

   # "d db -> g u s ub sb",
   # "d d  -> g d d u ub",

   # "d db -> u ub s sb c cb",

   # "u ub -> e- e+",
   # "u ub -> e- e+ g",
   # "u ub -> e- e+ g g",
   # "u ub -> e- e+ g g g",
   # "u ub -> e- e+ g g g g",
]

def extract_final_xs_hdf5(filename):
   xs_mean = 0.
   xs_sigma = 0.
   if os.path.exists(filename):
      try:
         data = hp.File(filename)
         xs_mean = data['generatedResult'][0]
         xs_sigma = data['generatedResult'][1]
         n_trials = data['runStatistics'][0]
         n_nonzero_after_cuts = data['runStatistics'][1]
         n_nonzero = data['runStatistics'][2]
      except:
         print('failed to open file: ',filename)
   return {
      'xs_mean': xs_mean,
      'xs_sigma': xs_sigma,
      'n_trials': n_trials,
      'n_nonzero_after_cuts': n_nonzero_after_cuts,
      'n_nonzero': n_nonzero,
      'HDF5 Filename': filename,
   }

class PepperProcess:
   def __init__(self,proc_id: int, 
                exec: str, 
                label: str = "",
                form_data_path: str = os.environ.get('PEPPER_DATA_PATH','/lus/gila/projects/atlas_aesp_CNDA/pepper/2023-10-06/colorfiles'),
                openmp: bool = False,
                cuda: bool = False,
                rocm: bool = False,
                xpu: bool = False,
                openmpi: bool = False,
                thread_count = 0,
                **kwargs: Dict[str, Any]) -> None:
      self.config = {
            'process': 'g g -> t tb g g',
            'process_input_type': 'process',
            'batch_size': 256000,
            'n_batches': 100,
            'seed': 12345,
            'verbosity': 'info',
            'collision_energy': 13000,
            'mu2': 'm_t^2',
            'use_cached_results': True,
            'cache_path': os.environ.get('PEPPER_CACHE_PATH','/lus/gila/projects/atlas_aesp_CNDA/pepper/2023-10-06/pepper_cache'),
            'integrator': 'Chili(basic)',
            'diagnostic_output_enabled': True,
            'diagnostic_path': 'pepper_diagnostics',
      }
      self.proc_id = proc_id
      self.exec = exec
      self.label = label
      self.form_data_path = form_data_path
      self.dev_id = -1
      self.openmp = openmp
      self.cuda = cuda
      self.rocm = rocm
      self.xpu = xpu
      self.openmpi = openmpi

      self.process_string = kwargs['process']
      self.process_formatted_string = self.process_string.replace(' ','_').replace('->','')
      if kwargs['process'] in process_configs:
         proc_conf = process_configs[kwargs['process']]
         if 'process_file' in proc_conf:
            kwargs['process'] = proc_conf['process_file']
            kwargs['process_input_type'] = 'process_file'
         if 'pepper_cache' in proc_conf:
            kwargs['cache_path'] = proc_conf['pepper_cache']
         if 'mu2' in proc_conf:
            kwargs['mu2'] = proc_conf['mu2']

      self.config.update(kwargs)
      self.config_filename = 'config.ini'
      self.stdout = ''
      self.stderr = ''
      self.return_code = None
      self.start_time = None
      self.end_time = None
      self.parsed_output = None
      self.thread_count = thread_count

      if 'hopper' in hostname:
         self.config['diagnostic_path'] += '_h100'
      self.config["diagnostic_path"] += "/" + self.process_formatted_string
      if len(label) > 0:
         self.config['diagnostic_path'] += "/" + label
      self.config['diagnostic_path'] += "/" + ("%05d" % proc_id)
      os.makedirs(self.config['diagnostic_path'],exist_ok=True)
    
   def write_config_file(self):

      if self.dev_id >= 0:
         self.config_filename = self.config['diagnostic_path'] + f'/config{self.dev_id:03d}.ini'
      else:
         self.config_filename = self.config['diagnostic_path'] + f'/config.ini'
      
      # Generate the configuration file content
      config_file_content = '''[main]
{process_input_type} = {process}
batch_size = {batch_size}
n_batches = {n_batches}
seed = {seed}
collision_energy = {collision_energy}
verbosity = {verbosity}
mu2 = {mu2}
max_overweight_xs_contribution = 0.01

[phase_space]
use_cached_results = {use_cached_results}
cache_path = {cache_path}
integrator = {integrator}


[events]
output_path = {diagnostic_path}/event_data.hdf5


[phase_space.optimisation]
n_iter = 7
n_nonzero_min = 30000
n_nonzero_min_growth_factor = 1.44

[phase_space.integration]
n_iter = 3
n_nonzero_min = 174000
n_nonzero_min_growth_factor = 1

[cuts]
jets.pt_min = 20
jets.nu_max = 5.0
jets.dR_min = 0.4

[dev]
diagnostic_output_enabled = {diagnostic_output_enabled}
diagnostic_path = {diagnostic_path}
'''.format(**self.config)
        
      # Write the configuration file
      with open(self.config_filename, 'w') as f:
         f.write(config_file_content)

   def launch(self) -> subprocess.Popen:
      self.write_config_file()
      # Run the Kokkos application as a subprocess
      subenv = os.environ
      if self.dev_id >= 0 and self.cuda:
         subenv['CUDA_VISIBLE_DEVICES'] = str(self.dev_id)
      elif self.dev_id >= 0 and self.rocm:
         subenv['ROCR_VISIBLE_DEVICES'] = str(self.dev_id)
      elif self.dev_id >= 0 and self.xpu:
         subenv['ZE_AFFINITY_MASK'] = str(self.dev_id)
      elif self.openmp:
         subenv['OMP_NUM_THREADS'] = str(self.thread_count)
         subenv['OMP_PROC_BIND'] = 'spread'
         subenv['OMP_PLACES'] = 'threads'
      subenv['PEPPER_DATA_PATH'] = self.form_data_path
      logger.info("launching pepper process '%s' / %d with dev_id: %d  directory: %s",self.process_string,self.config['batch_size'],self.dev_id,self.config['diagnostic_path'])
      self.start_time = time.time()
      exec = self.exec
      if self.openmpi:
         exec = 'mpiexec -n ' + str(self.thread_count) + ' --oversubscribe ' + self.exec
      exec += ' ' + self.config_filename
      self.process = subprocess.Popen(exec.split(' '),env=subenv, 
                                      stdout=open(os.path.join(self.config['diagnostic_path'],'stdout.txt'),'w'),
                                      stderr=open(os.path.join(self.config['diagnostic_path'],'stderr.txt'),'w'))
      logger.debug('started %s  with PID: %d',str(self),self.process.pid)
      return self.process

   def check(self, run_limit: float) -> Tuple[bool, List[str]]:
      """
      Checks if the process is done and returns the results.

      Args:
         run_limit: The maximum amount of time (in seconds) to allow
            the process to run before killing it.

      Returns:
         A tuple of (is_done, results). `is_done` is True if the process is done,
         and False if the process is still running or was killed. `results` is a
         list of the stdout, stderr, and return code of the process.
      """

      # Check if the process is done
      if self.process.poll() is None:
         # Process is still running
         # check if run limit has been reached
         if time.time() - self.start_time > run_limit:
            self.process.terminate()
            logger.info('killing %d for running too long',self.proc_id)
         else:
            return False, []

      # Get stdout, stderr, and return code
      stdout_fn = os.path.join(self.config['diagnostic_path'],'stdout.txt')
      stdout = open(stdout_fn).read()
      stderr_fn = os.path.join(self.config['diagnostic_path'],'stderr.txt')
      stderr = open(stderr_fn).read()

      return_code = self.process.returncode
      self.end_time = time.time()
      self.duration = self.end_time - self.start_time

      self.stdout = stdout
      self.stderr = stderr
      self.return_code = return_code

      self.parse_output()
      
      # Return True and the results
      return True, [self.stdout, self.stderr, return_code]


   #     return result
   def read_timers_csv(csv_filename):
      timers = pd.read_csv(csv_filename)
      timers['Task'] = timers['Task'].apply(lambda x: x.strip())
      # Remove padding from column names
      timers.columns = timers.columns.str.strip()
      x = {
         'Total Runtime':                 timers.loc[timers['Task'] == 'Total','Duration [s]'].iloc[0],
         'Event Generation Runtime':      timers.loc[timers['Task'] == 'Event generation','Duration [s]'].iloc[0],
         'Optimisation Runtime':          timers.loc[timers['Task'] == 'Optimisation','Duration [s]'].iloc[0],
         'Initialisation Runtime':        timers.loc[timers['Task'] == 'Initialisation','Duration [s]'].iloc[0],
         'HDF5 Close Runtime':            timers.loc[timers['Task'] == 'HDF5 close','Duration [s]'].iloc[0],
         'HDF5 Open Runtime':             timers.loc[timers['Task'] == 'HDF5 open','Duration [s]'].iloc[0],
         'HDF5 Init Runtime':             timers.loc[timers['Task'] == 'HDF5 init','Duration [s]'].iloc[0],
         'Unweighting Setup Runtime':     timers.loc[timers['Task'] == 'Unweighting setup','Duration [s]'].iloc[0],
         'EG Recursion':                  timers.loc[timers['Task'] == 'Recursion','Duration [s]'].iloc[0],
         'EG Output':                     timers.loc[timers['Task'] == 'Output','Duration [s]'].iloc[0],
         'EG PDF and AlphaS evaluation':  timers.loc[timers['Task'] == 'PDF and AlphaS evaluation','Duration [s]'].iloc[0],
         'EG Phase space':                timers.loc[timers['Task'] == 'Phase space','Duration [s]'].iloc[0],
         'EG External State':             timers.loc[timers['Task'] == 'External state construction','Duration [s]'].iloc[0],
         'EG RNG':                        timers.loc[timers['Task'] == 'Random number generation','Duration [s]'].iloc[0],
         'EG-R Calculate currents':       timers.loc[timers['Task'] == 'Calculate currents','Duration [s]'].iloc[0],
         'EG-R Momenta preparation':      timers.loc[timers['Task'] == 'Momenta preparation','Duration [s]'].iloc[0],
         'EG-R Currents preparation':     timers.loc[timers['Task'] == 'Currents preparation','Duration [s]'].iloc[0],
         'EG-R IP reset':                 timers.loc[timers['Task'] == 'Internal particle information reset','Duration [s]'].iloc[0],
         'EG-R ME update':                timers.loc[timers['Task'] == 'ME update','Duration [s]'].iloc[0],
         'EG-R ME reset':                 timers.loc[timers['Task'] == 'ME reset','Duration [s]'].iloc[0],
         'EG-R IC reset':                 timers.loc[timers['Task'] == 'Internal currents reset','Duration [s]'].iloc[0],
         'EG-PDF PDF':                    timers.loc[timers['Task'] == 'PDF','Duration [s]'].iloc[0],
         'EG-PDF AlphaS':                 timers.loc[timers['Task'] == 'AlphaS','Duration [s]'].iloc[0],
         'EG-O Filter non-zero':          timers.loc[timers['Task'] == 'Filter non-zero events','Duration [s]'].iloc[0],
         'EG-O Pull non-zero':            timers.loc[timers['Task'] == 'Pull non-zero events','Duration [s]'].iloc[0],
         'EG-O HDF5 write':               timers.loc[timers['Task'] == 'HDF5 write','Duration [s]'].iloc[0],
         'EG-PS Fill Mom Weights':        timers.loc[timers['Task'] == 'Fill momenta and weights','Duration [s]'].iloc[0],
         'EG-PS Update weights':          timers.loc[timers['Task'] == 'Update weights','Duration [s]'].iloc[0],
         'EG-PS Cuts':                    timers.loc[timers['Task'] == 'Cuts','Duration [s]'].iloc[0],
         'CSV Timer Filename':            csv_filename,
      }
      try:
         x['EG-R ME2 update']=            timers.loc[timers['Task'] == 'ME2 update','Duration [s]'].iloc[0]
      except:
         x['EG-R ME2 update']= 0
      return x

   def parse_output(self):
      # combine config and output info 
      output = {}
      output['process_string'] = self.process_string
      output.update(self.config)
      output['proc_id'] = self.proc_id
      output["label"] = self.label
      output['run_time'] = self.duration
      output['return_code'] = self.return_code
      output['thread_count'] = self.thread_count
      # parse timers.csv file
      try:
         timers_filename = glob.glob(self.config['diagnostic_path'] + "/*/timers.csv")
         if len(timers_filename) > 1:
            raise Exception("multiple timers.csv files in job: %s" % str(self))
         if len(timers_filename) == 0:
            raise Exception("no timers.csv files in job: %s" % str(self))
         else:
            timers_filename = timers_filename[0]
         output.update(PepperProcess.read_timers_csv(timers_filename))
      except:
         logger.exception('failed to get timers.csv for %d',self.proc_id)
      # parse event data HDF5 file
      try:
         event_data_file = glob.glob(os.path.join(self.config['diagnostic_path'],"event_data*.hdf5"))[0]
         event_data_size = os.path.getsize(event_data_file)
         output['event_data_size'] = event_data_size
         output.update(extract_final_xs_hdf5(event_data_file))
         os.remove(event_data_file)
      except:
         logger.exception("failed to get file size for event data in job: %s",str(self))
         output['event_data_size'] = 0
      
      try:
         output['event_period'] = output['Event Generation Runtime'] / output['batch_size'] / output['n_batches']
         output['event_rate'] = 1. / output['event_period']
      except:
         logger.exception('failed to get event rate for %d: %s',self.proc_id,str(self))
         output['event_rate'] = 0.
         output['event_period'] = 0.

      self.parsed_output = output
   
   def __str__(self):
      return f"{self.proc_id} {self.process_string} {self.thread_count} {self.config['batch_size']} {self.config['n_batches']}"


class DeviceManager:
   def __init__(self,num_concurrent: int) -> None:
      self.num_concurrent = num_concurrent
      self.available_devices = list(range(num_concurrent))
   def append(self,process: PepperProcess):
      index = self.available_devices.pop()
      process.dev_id = index
   def pop(self,process: PepperProcess):
      self.available_devices.append(process.dev_id)

# from ChatGPT
class DataLogger:
   def __init__(self, filename):
      self.filename = filename
      self.used_proc_ids = []
      if os.path.exists(self.filename):
         self.dataframe = pd.read_csv(self.filename)
         self.load_proc_ids()
      else:
         self.dataframe = None
   
   def load_proc_ids(self):
      if 'proc_id' not in self.dataframe.columns:
         self.dataframe['proc_id'] = self.dataframe['diagnostic_path'].apply(lambda x: int(x.split('/')[-1]))
      self.used_proc_ids = self.dataframe['proc_id'].tolist()
   
   def get_next_proc_id(self):
      proc_id = 0
      while proc_id in self.used_proc_ids:
         proc_id += 1
      self.used_proc_ids.append(proc_id)
      return proc_id

   def append(self, data_dict):
      # If dataframe is not yet initialized, create a new one
      if self.dataframe is None:
         self.dataframe = pd.DataFrame([data_dict])
      else:
         # Append the data_dict to the existing dataframe
         new_row = pd.DataFrame([data_dict])
         self.dataframe = pd.concat([self.dataframe, new_row], ignore_index=True)

      # Write the updated dataframe to the CSV file
      self.dataframe.sort_values(by=['process_string','batch_size'],inplace=True)
      self.dataframe.to_csv(self.filename, index=False)
   
   def remove_duplicates(self,proc_list):
      attr_to_check = [
            'process',
            'batch_size',
            'n_batches',
            'seed',
            'collision_energy',
      ]
      pruned_list = []
      for proc in proc_list:
         # check that all attributes are present
         all_attr_present = True
         for attr in attr_to_check:
            if attr not in proc.config.keys():
               all_attr_present = False
         
         combined_or = self.dataframe[attr_to_check[0]] == proc.config[attr_to_check[0]]
         for attr in attr_to_check:
            combined_or = combined_or & (self.dataframe[attr] == proc.config[attr])

         if not combined_or.any():
            pruned_list.append(proc)
      
      return pruned_list

def run_multiple_kokkos_apps(process_strings: List[str], 
                             batch_sizes: List[Tuple[int, int]],
                             exec: str,
                             num_concurrent: int,
                             run_limit: int, 
                             enable_cuda: bool = False,
                             openmp: bool = False,
                             enable_rocm: bool = False,
                             enable_xpu: bool = False,
                             openmpi: bool = False,
                             output_file: str = 'output.csv',
                             retry: bool = False,
                             skip_done: bool = False,
                             label: str = '') -> None:

   if enable_cuda or enable_rocm or enable_xpu:
      device = DeviceManager(num_concurrent)

   datalog = DataLogger(output_file)
   pepper_procs = []
   
   # populate process objects:
   for process_string in process_strings:

      for thread_count,batch_size in batch_sizes:
         
         n_batches = get_n_batches(process_string,batch_size,enable_cuda or enable_rocm or enable_xpu)
         
         tmp_proc = PepperProcess(datalog.get_next_proc_id(),exec,label,
                                    openmp=openmp,
                                    cuda=enable_cuda,rocm=enable_rocm,xpu=enable_xpu,openmpi=openmpi,
                                    thread_count=thread_count,
                                    process=process_string,
                                    batch_size=batch_size,
                                    n_batches=n_batches)
         pepper_procs.append(tmp_proc)
   
   # skip already completed processes
   if skip_done and datalog.dataframe is not None:
      pepper_procs = datalog.remove_duplicates(pepper_procs)
   
   # add failed processes to the list if specified
   if retry and datalog.dataframe is not None:
      failed = datalog.dataframe[datalog.dataframe['return_code'] != 0]
      for index,row in failed.iterrows():
         proc_id = row['proc_id']
         tmp_proc = PepperProcess(proc_id,exec,label,
                                  openmp=openmp,
                                  cuda=enable_cuda,rocm=enable_rocm,xpu=enable_xpu,openmpi=openmpi,
                                  thread_count=row['thread_count'],
                                  process=row['process'],
                                  batch_size=row['batch_size'])
         pepper_procs.append(tmp_proc)

   logger.info('running these processes:')
   for proc in pepper_procs:
      logger.info("   %s",proc)
   # sys.exit(0)

   running_processes = []
   completed_processes = []

   for pepper_proc in pepper_procs:
      # Launch new processes until reaching the maximum concurrency
      while len(running_processes) >= num_concurrent:
         for process in running_processes:
            is_done, results = process.check(run_limit)
            if is_done:
               done_process_string = process.process_string
               logger.info("%25s exited with %5d and ran for %5f seconds",str(process),results[2],process.duration)
               if results[2] != 0:
                  print(results[1])
               completed_processes.append(results)
               running_processes.remove(process)
               logger.info("completed %5d out of %5d processes.",len(completed_processes),len(pepper_procs))

               if enable_cuda or enable_rocm or enable_xpu:
                  device.pop(process)
               
               datalog.append(process.parsed_output)
               break
            else:
               logger.debug("%s still running",process.proc_id)
         else:
            time.sleep(1)  # Wait for a second before checking again

      # Launch the application subprocess
      if enable_cuda or enable_rocm or enable_xpu:
         device.append(pepper_proc)
      pepper_proc.launch()
      running_processes.append(pepper_proc)
    
   # Wait for the remaining processes to complete
   while len(running_processes) > 0:
      for process in running_processes:
         is_done, results = process.check(run_limit)
         if is_done:
            done_process_string = process.process_string
            logger.info("%15s exited with %5d and ran for %5f seconds",str(process),results[2],process.duration)
            logger.info("completed %5d out of %5d processes.",len(completed_processes),len(pepper_procs))
            completed_processes.append(results)
            running_processes.remove(process)

            if enable_cuda or enable_rocm or enable_xpu:
               device.pop(process)
            datalog.append(process.parsed_output)
            break
         else:
            logger.debug("%s still running",process.proc_id)
      else:
         time.sleep(1)  # Wait for a second before checking again
    

def main():
   ''' simple starter program that can be copied for use when starting a new script. '''
   logging_format = '%(asctime)s %(levelname)s:%(name)s:%(message)s'
   logging_datefmt = '%Y-%m-%d %H:%M:%S'
   logging_level = logging.INFO
   
   parser = argparse.ArgumentParser(description='')
   parser.add_argument('-c','--concur',help='number of concurrent processes',type=int,required=True)
   parser.add_argument('-r','--run-limit',help='limit each app to only run for this number of sections',type=int,default=10000)

   parser.add_argument('-o','--output',help='output csv file',default='pepper_runs.csv')
   parser.add_argument('-e','--exec',help='path to executable to use',required=True)

   parser.add_argument('-l','--label',help='hardware label like "V100" or "MI250" or "Skylake"',required=True)

   parser.add_argument('--cuda', default=False, action='store_true', help="enable cuda")
   parser.add_argument('--openmp', default=False, action='store_true', help="enable openmp")
   parser.add_argument('--rocm', default=False, action='store_true', help="enable rocm")
   parser.add_argument('--xpu', default=False, action='store_true', help="enable Intel GPU")
   parser.add_argument('--openmpi', default=False, action='store_true', help="enable OpenMPI, only works for CUDA/Serial-CPU version")

   parser.add_argument('--retry', default=False, action='store_true', help="retry entries in output file with nonzero return codes")
   parser.add_argument('--skip-done', default=False, action='store_true', help="run again but skip processes that have already run")
   

   parser.add_argument('--debug', dest='debug', default=False, action='store_true', help="Set Logger to DEBUG")
   parser.add_argument('--error', dest='error', default=False, action='store_true', help="Set Logger to ERROR")
   parser.add_argument('--warning', dest='warning', default=False, action='store_true', help="Set Logger to ERROR")
   parser.add_argument('--logfilename',dest='logfilename',default=None,help='if set, logging information will go to file')
   args = parser.parse_args()

   if args.debug and not args.error and not args.warning:
      logging_level = logging.DEBUG
   elif not args.debug and args.error and not args.warning:
      logging_level = logging.ERROR
   elif not args.debug and not args.error and args.warning:
      logging_level = logging.WARNING

   logging.basicConfig(level=logging_level,
                       format=logging_format,
                       datefmt=logging_datefmt,
                       filename=args.logfilename)

   # the batch sizes is a set ( thread_count, batch_size )
   #  this was needed for OpenMP which needs OMP_NUM_THREADS
   #  set to a hardware compatible value, while the batch_size
   #  can be larger to save on data motion. 
   #  For GPUs, I expect these values to be identical

   if not os.path.exists(os.environ["PEPPER_DATA_PATH"]):
      logger.error("PEPPER_DATA_PATH not set")
      sys.exit(1)
   else:
      logger.info("PEPPER_DATA_PATH set to %s",os.environ["PEPPER_DATA_PATH"])
   # for CUDA
   if args.cuda or args.rocm or args.xpu:
      batch_sizes = [ (2 ** i, 2 ** i) for i in range(7,21)]
   # for OpenMP
   elif args.openmp:
      # for 2x Skylake 8180
      #   batch_sizes = [8,16,32,48,56,64,128]
      threads = [32,48,56,112]
      batch_sizes = []
      for i in range(0,11,2):
         scaler = 2 ** i
         for thread_count in threads:
            batch_sizes.append((thread_count,thread_count*scaler))
   elif args.openmpi:
      # for serial, single node runs with OpenMPI
      threads = [16,32,48,56,112]
      batch_sizes = []
      for batch_size in [50,100,250]:
         for thread_count in threads:
            batch_sizes.append((thread_count,batch_size))
   
   global processes
   processes = sorted(processes)

   run_multiple_kokkos_apps(processes,batch_sizes,args.exec,args.concur, args.run_limit, 
                            args.cuda,args.openmp,args.rocm,args.xpu,args.openmpi,args.output,
                            args.retry,args.skip_done,args.label)



def get_n_batches(process_string,batch_size,on_gpu):
   outgoing_string = process_string[process_string.find('->')+2:]
   n_jets = outgoing_string.count(" j")
   n_gluons = outgoing_string.count(" g")
   n_jets += n_gluons
   if on_gpu:
      if   n_jets == 5:
         n_batches = 25
      elif n_jets == 4:
         n_batches = 1e2
      elif n_jets == 3:
         n_batches = 1e3
      elif n_jets == 2:
         n_batches = 10e3
      elif n_jets == 1:
         n_batches = 15e3
      elif n_jets == 0:
         n_batches = 20e3
      else:
         logger.warning("gpu: no n_batches set for process: %s",process_string)

      if batch_size >= 1024:
         n_batches *= 0.9
      if batch_size >= 2048:
         n_batches *= 0.8
      if batch_size >= 4096:
         n_batches *= 0.7
      if batch_size >= 8192:
         n_batches *= 0.7
      if batch_size >= 16384:
         n_batches *= 0.7
      if batch_size >= 32768:
         n_batches *= 0.8
      if batch_size >= 65536:
         n_batches *= 0.8
      if batch_size >= 131072:
         n_batches *= 0.8
      if batch_size >= 262144:
         n_batches *= 0.8
      if batch_size >= 524288:
         n_batches *= 0.9
      if batch_size >= 1048576:
         n_batches *= 0.3

   else:
      
      if   n_jets == 5:
         n_batches = 25
      elif n_jets == 4:
         n_batches = 1e2
      elif n_jets == 3:
         n_batches = 1e3
      elif n_jets == 2:
         n_batches = 5e3
      elif n_jets == 1:
         n_batches = 10e3
      elif n_jets == 0:
         n_batches = 15e3
      else:
         logger.warning("cpu: no n_batches set for process: %s",process_string)

      if batch_size >= 64:
         n_batches *= 0.8
      if batch_size >= 128:
         n_batches *= 0.8
      if batch_size >= 250:
         n_batches *= 0.8
      if batch_size >= 500:
         n_batches *= 0.8
      if batch_size >= 750:
         n_batches *= 0.8
      if batch_size >= 1000:
         n_batches *= 0.8
      if batch_size >= 2500:
         n_batches *= 0.7
      if batch_size >= 5000:
         n_batches *= 0.7
      if batch_size >= 7500:
         n_batches *= 0.7
      if batch_size >= 10000:
         n_batches *= 0.7
      if batch_size >= 15000:
         n_batches *= 0.7
      if batch_size >= 25000:
         n_batches *= 0.6
      if batch_size >= 35000:
         n_batches *= 0.6
      if batch_size >= 50000:
         n_batches *= 0.5

   n_batches = max(int(n_batches),10)
   
   return n_batches



if __name__ == "__main__":
   main()
