import argparse
import logging
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import numpy as np
logger = logging.getLogger(__name__)

color_map = {
   '2xSkylake8180': 'purple',
   '2xSkylake8180-2': 'yellow',
   'V100': 'lightsteelblue',
   'A100': 'blue',
   'H100': 'royalblue',
   'MI250': 'lime',
   'MI100': 'green',
   'A100 (CUDA)': 'red',
   'V100 (CUDA)': 'red',
   '2xSkylake8180 (CUDA)': 'red',
   'PVC': 'orange',
}

def plot_data(df,output_path,x_col='batch_size',y_col='event_rate',label_col='impl',ylabel='Event Rate [1/s]',xlabel='Thread Count',fn_extensions=['png']):
   processes = df['process_string'].unique()
   
   for process in processes:
      process_df = df[df['process_string'] == process]
      # First figure with "Event Rate [1/s]"
      plt.figure(figsize=(12, 8), dpi=120)
      for label, label_df in process_df.groupby(label_col):
         label_df.sort_values(by='batch_size',inplace=True)
         linestyle='solid'
         if 'CUDA' in label:
            linestyle='dashed'
         plt.plot(label_df[x_col], label_df[y_col], label=label,linestyle=linestyle, color=color_map[label],marker='o')
      plt.xlabel(xlabel)
      plt.ylabel(ylabel)
      plt.title(process)
      plt.legend()
      plt.xscale('log')
      plt.yscale('log')
      plt.grid(True, which='both', axis='y')
      plt.grid(True, which='major', linestyle='-', alpha=0.7)
      plt.grid(True, which='minor', linestyle='-.', alpha=0.4)
      for fn_extension in fn_extensions:
         output_fn = os.path.join(output_path, f'{process.replace(" ","_").replace("->", "to")}_{y_col.replace(" ","_")}.{fn_extension}')
         plt.savefig(output_fn)
      plt.close()


def plot_rate_period(combined_df,output_path):

   combined_df['me_rate'] = combined_df['event_rate']
   combined_df['me_period'] = combined_df['event_period']

   combined_df['unw_evt_rate'] = combined_df['n_nonzero'] / combined_df['Event Generation Runtime']
   combined_df['unw_evt_period'] = combined_df['Event Generation Runtime'] / combined_df['n_nonzero']

   combined_df['bytes_per_event'] = combined_df['event_data_size'] / combined_df['n_nonzero']

   combined_df['unweightingEfficiency'] = combined_df['n_nonzero'] / combined_df['n_nonzero_after_cuts']

   kokkos_df = combined_df[combined_df['reference'] == False]

   plot_data(combined_df,output_path,x_col='batch_size',y_col='me_rate',label_col='impl',ylabel='Matrix Element Rate [1/s]',xlabel='Thread Count',fn_extensions=['png','pdf'])
   plot_data(combined_df,output_path,x_col='batch_size',y_col='me_period',label_col='impl',ylabel='Matrix Element Period [s]',xlabel='Thread Count',fn_extensions=['png','pdf'])
   idx = plot_max_comparison(combined_df,output_path,y_col='me_rate',ylabel='Matrix Element Rate [1/s]',fn_extensions=['png','pdf'])
   # plot_comparison_for_idx(kokkos_df,idx,output_path,y_col='unweightingEfficiency',ylabel='Unweighting Efficiency [%]')

   plot_data(combined_df,output_path,x_col='batch_size',y_col='unw_evt_rate',label_col='impl',ylabel='Unw. Event Rate [1/s]',xlabel='Thread Count',fn_extensions=['png','pdf'])
   plot_data(combined_df,output_path,x_col='batch_size',y_col='unw_evt_period',label_col='impl',ylabel='Unw. Event Period [s]',xlabel='Thread Count',fn_extensions=['png','pdf'])
   idx = plot_max_comparison(combined_df,output_path,y_col='unw_evt_rate',ylabel='Unw. Event Rate [1/s]',fn_extensions=['png','pdf'])
   plot_comparison_for_idx(combined_df,idx,output_path,y_col='unweightingEfficiency',ylabel='Unweighting Efficiency [%]',fn_extensions=['png','pdf'])
   plot_comparison_for_idx(combined_df,idx,output_path,y_col='n_nonzero',ylabel='n_nonzero',fn_extensions=['png','pdf'])
   plot_comparison_for_idx(combined_df,idx,output_path,y_col='n_trials',ylabel='n_trials',fn_extensions=['png','pdf'])
   plot_comparison_for_idx(combined_df,idx,output_path,y_col='bytes_per_event',ylabel='Event Size [bytes]',fn_extensions=['png','pdf'])

   # Calculate the fraction for each runtime column
   top_level_columns = ['Event Generation Runtime',
                        'Optimisation Runtime',
                        'Initialisation Runtime',
                        'HDF5 Close Runtime',
                        'HDF5 Open Runtime',
                        'HDF5 Init Runtime',
                        'Unweighting Setup Runtime'
                       ]

   event_generation_columns = ['EG Recursion',
                               'EG Output',
                               'EG PDF and AlphaS evaluation',
                               'EG Phase space',
                               'EG External State',
                               'EG RNG'
                              ]

   recursion_columns = ["EG-R Calculate currents",
                        "EG-R Momenta preparation",
                        "EG-R Currents preparation",
                        "EG-R IP reset",
                        "EG-R ME update",
                        "EG-R ME2 update",
                        "EG-R ME reset",
                        "EG-R IC reset"]


   pdf_columns = [
                  "EG-PDF PDF",
                  "EG-PDF AlphaS",
                 ]
   
   output_columns = [
                     "EG-O Filter non-zero",
                     "EG-O Pull non-zero",
                     "EG-O HDF5 write",
                    ]
   
   ps_columns = [
                  "EG-PS Fill Mom Weights",
                  "EG-PS Update weights",
                  "EG-PS Cuts",
                  ]
   
   idx_plot_runtime_distribution(combined_df,idx,output_path,'runtime_fraction',top_level_columns,'Total Runtime',fn_extensions=['png','pdf'])
   idx_plot_runtime_distribution(combined_df,idx,output_path,'runtime_fraction.eg',event_generation_columns,'Event Generation Runtime',fn_extensions=['png','pdf'])
   idx_plot_runtime_distribution(combined_df,idx,output_path,'runtime_fraction.eg-r',recursion_columns,'EG Recursion',fn_extensions=['png','pdf'])
   idx_plot_runtime_distribution(combined_df,idx,output_path,'runtime_fraction.eg-pdf',pdf_columns,'EG PDF and AlphaS evaluation',fn_extensions=['png','pdf'])
   idx_plot_runtime_distribution(combined_df,idx,output_path,'runtime_fraction.eg-o',output_columns,'EG Output',fn_extensions=['png','pdf'])
   idx_plot_runtime_distribution(combined_df,idx,output_path,'runtime_fraction.eg-ps',ps_columns,'EG Phase space',fn_extensions=['png','pdf'])
   

def plot_max_comparison(combined_df,output_path,y_col='event_rate',ylabel='Event Rate [1/s]',fn_extensions=['png']):

   max_rate_df_idx = combined_df.groupby(['process_string', 'impl'])[y_col].idxmax()
   print('%25s %20s %20s %20s %20s' % ('Process', 'Label', 'Batch Size', 'Thread Count', y_col))
   for (process, label), idx in max_rate_df_idx.items():
      row = combined_df.loc[idx]
      print('%25s %20s %20d %20d %20f' % (process, label, row['batch_size'], row['thread_count'], row[y_col]))

   max_rate_df = combined_df.groupby(['process_string', 'impl'])[y_col].max().reset_index()

   processes = max_rate_df['process_string'].unique()
   labels = max_rate_df['impl'].unique()
   print(labels)
   labels = [item for item in color_map.keys() if item in labels]

   x = np.arange(len(processes))  # the label locations
   width = 0.8 / len(labels)  # the width of the bars

   fig, ax = plt.subplots(figsize=(12, 8), dpi=120)
   for i, label in enumerate(labels):
      label_df = max_rate_df[max_rate_df['impl'] == label]
      tmp_x = x + i * width
      tmp_y = [0. for _ in range(len(tmp_x))]
      tmp_y[0:len(label_df[y_col])] = label_df[y_col].to_list()
      ax.bar(tmp_x, tmp_y, width, label=label, color=color_map[label])

   # Add some text for labels, title and custom x-axis tick labels, etc.
   ax.set_ylabel(ylabel)
   ax.set_xticks(x)
   ax.set_xticklabels(processes, rotation=35)
   ax.legend()
   ax.set_yscale('log')
   ax.grid(True, which='both', axis='y')
   ax.grid(True, which='major', linestyle='-', alpha=0.7)
   ax.grid(True, which='minor', linestyle='-.', alpha=0.4)
   plt.minorticks_on()
   #  ax.get_yaxis().set_major_formatter(ScalarFormatter())

   fig.tight_layout()
   for fn_extension in fn_extensions:
      output_fn = os.path.join(output_path, f'max_{y_col.replace(" ","_")}.{fn_extension}')
      plt.savefig(output_fn)
   plt.close()

   return max_rate_df_idx

def plot_comparison_for_idx(combined_df,idx,output_path,y_col='event_rate',ylabel='Event Rate [1/s]',fn_extensions=['png']):

   comp_df = combined_df.loc[idx]

   processes = comp_df['process_string'].unique()
   labels = comp_df['impl'].unique()
   labels = [item for item in color_map.keys() if item in labels]

   x = np.arange(len(processes))  # the label locations
   width = 0.8 / len(labels)  # the width of the bars

   fig, ax = plt.subplots(figsize=(12, 8), dpi=120)
   for i, label in enumerate(labels):
      label_df = comp_df[comp_df['impl'] == label]
      tmp_x = x + i * width
      tmp_y = [0. for _ in range(len(tmp_x))]
      tmp_y[0:len(label_df[y_col])] = label_df[y_col].to_list()
      ax.bar(tmp_x, tmp_y, width, label=label, color=color_map[label])

   # Add some text for labels, title and custom x-axis tick labels, etc.
   ax.set_ylabel(ylabel)
   ax.set_xticks(x)
   ax.set_xticklabels(processes, rotation=35)
   ax.legend()
   ax.set_yscale('log')
   ax.grid(True, which='both', axis='y')
   ax.grid(True, which='major', linestyle='-', alpha=0.7)
   ax.grid(True, which='minor', linestyle='-.', alpha=0.4)
   plt.minorticks_on()
   #  ax.get_yaxis().set_major_formatter(ScalarFormatter())

   fig.tight_layout()
   for fn_extension in fn_extensions:
      output_fn = os.path.join(output_path, f'idxcomp_{y_col.replace(" ","_")}.{fn_extension}')
      plt.savefig(output_fn)
   plt.close()


def idx_plot_runtime_distribution(combined_df,idx,output_path,output_name,runtime_cols,ref_col,fn_extensions=['png']):
   
   max_rate_df = combined_df.loc[idx]

   processes = max_rate_df['process_string'].unique()
   labels = max_rate_df['impl'].unique()

   # Loop through each label and create a plot
   for label in labels:
      # Filter data for the current label
      df_label = max_rate_df[max_rate_df['impl'] == label]
      df_frac = df_label[runtime_cols].div(df_label[ref_col], axis=0)
      
      # Plot
      fig, ax = plt.subplots(figsize=(12, 8), dpi=120)
      df_frac.plot(kind='bar', stacked=True, figsize=(10, 6),ax=ax)
      ax.set_title(f'{label}')
      ax.set_xlabel('')
      ax.set_ylabel('Fraction of Runtime')
      ax.set_ylim(0, 1)
      ax.set_xticks(ticks=range(len(processes)), labels=processes, rotation=30, ha='right')
      ax.legend(title='Runtime Type', bbox_to_anchor=(1.05, 1), loc='upper left')
      fig.tight_layout()
      for fn_extension in fn_extensions:
         fig.savefig(os.path.join(output_path, f'{output_name}.{label}.{fn_extension}'))
      plt.close()

def compare_implementations(df):
   
   df_kokkos = df[df['reference'] == False]

   # Load the data
   df_cuda = df[df['reference'] == True]

   # Ensure CUDA dataframe only contains V100 results
   # df_cuda = df_cuda[df_cuda['label'] == 'V100']

   # Get the unique processes
   processes = df_kokkos['process_string'].unique()

   process_str_len = 0
   for process in processes:
      process_str_len = max(process_str_len, len(process))
   process_str_format = f"%-{process_str_len}s"
   # Loop over each process
   for process in processes:
      # Get the data for this process
      df_kokkos_process = df_kokkos[df_kokkos['process_string'] == process]
      df_cuda_process = df_cuda[df_cuda['process_string'] == process]

      # If there is no data for this process, skip this iteration
      if df_kokkos_process.empty or df_cuda_process.empty:
         continue

      # Get the cross section and its uncertainty for Kokkos and CUDA
      min_row_idx = df_kokkos_process[df_kokkos_process['xs_mean'] > 0]['xs_sigma'].idxmin()
      min_row = df_kokkos_process.loc[min_row_idx]
      cross_section_kokkos = min_row['xs_mean']
      uncertainty_kokkos = min_row['xs_sigma']

      min_row_idx = df_cuda_process[df_cuda_process['xs_sigma'] > 0]['xs_sigma'].idxmin()
      min_row = df_cuda_process.loc[min_row_idx]
      cross_section_cuda = min_row['xs_mean']
      uncertainty_cuda = min_row['xs_sigma']

      # Calculate the relative difference and the total uncertainty
      relative_difference = abs(cross_section_kokkos - cross_section_cuda) / cross_section_cuda
      total_uncertainty = (uncertainty_kokkos**2 + uncertainty_cuda**2)**0.5 / cross_section_cuda

      # Check if the relative difference is less than the total uncertainty
      process_str = process_str_format % process
      if relative_difference <= total_uncertainty:
         print(f"The cross sections for process '{process_str}'        agree statistically; Kokkos: {cross_section_kokkos:8.6g} +- {uncertainty_kokkos:8.6g}; CUDA: {cross_section_cuda:8.6g} +- {uncertainty_cuda:8.6g}")
      else:
         print(f"The cross sections for process '{process_str}' do not agree statistically; Kokkos: {cross_section_kokkos:8.6g} +- {uncertainty_kokkos:8.6g}; CUDA: {cross_section_cuda:8.6g} +- {uncertainty_cuda:8.6g}")


def plot_cross_sections(df,output_path,fn_extensions):

   # Filter out batch sizes in which the cross section was zero
   df = df[df['xs_mean'] != 0]

   # Get the unique processes
   processes = df['process_string'].unique()

   # Loop over each process
   for process in processes:
      # Create a new figure
      plt.figure(figsize=(12, 8), dpi=120)

      # Get the data for this process
      df_process = df[df['process_string'] == process]

      # Get the unique implementations
      implementations = df_process['impl'].unique()
      
      # Loop over each implementation
      for implementation in implementations:
         # Get the data for this implementation
         df_implementation = df_process[df_process['impl'] == implementation]

         # Sort by batch size
         df_implementation = df_implementation.sort_values('batch_size')

         # Plot the cross section with uncertainty bounds
         if 'CUDA' in implementation:
            plt.errorbar(df_implementation['batch_size'], df_implementation['xs_mean'],
                         yerr=df_implementation['xs_sigma'],
                         fmt='o', color='red', label=implementation)
         else:
            plt.plot(df_implementation['batch_size'], df_implementation['xs_mean'], label=implementation, color=color_map[implementation])
            plt.fill_between(df_implementation['batch_size'], 
                             df_implementation['xs_mean'] - df_implementation['xs_sigma'], 
                             df_implementation['xs_mean'] + df_implementation['xs_sigma'], alpha=0.2, color=color_map[implementation])

      # Add a grid
      plt.grid(which='both')
      plt.minorticks_on()
      plt.grid(which='minor', alpha=0.2, linestyle=':')
      plt.grid(which='major', alpha=0.5)

      # Set log scale
      plt.xscale('log')
      plt.yscale('log')

      # Set the title and labels
      plt.title(process, fontsize=18)
      plt.xlabel('Batch Size', fontsize=18)
      plt.ylabel('Cross Section', fontsize=18)
      plt.legend()

      # Save the figure
      if not os.path.exists('plots'):
         os.makedirs('plots')
      plt.tight_layout()
      for fn_extension in fn_extensions:
         plt.savefig(f'{output_path}/{process.replace(" ", "_").replace("->", "to")}_xs.{fn_extension}')

      # Close the figure to free up memory
      plt.close()


import glob
from pptx import Presentation
from pptx.util import Inches

def create_ppt_from_images(output_file,output_path):
   # Get a list of PNG files matching the glob string
   xs_image_files = sorted(glob.glob(os.path.join(output_path,'*_xs.png')))
   rate_image_files = sorted(glob.glob(os.path.join(output_path,'*_rate.png')))
   period_image_files = sorted(glob.glob(os.path.join(output_path,'*_period.png')))
   max_event_rate_file = os.path.join(output_path,'max_event_rate.png')
   runtime_dist = sorted(glob.glob(os.path.join(output_path,'max_runtime_distribution*.png')))
   
   # Create a new PowerPoint presentation
   presentation = Presentation()
   
   # Calculate the number of images per slide (4 images in a 2x2 grid)
   images_per_slide = 4
   num_slides = len(xs_image_files) 

   # Create a new slide
   slide_layout = presentation.slide_layouts[6]  # Layout for a blank slide
   slide = presentation.slides.add_slide(slide_layout)
   slide.shapes.add_picture(max_event_rate_file, Inches(0.25), Inches(0.25), width=Inches(9), height=Inches(7))
   
   # Iterate over the number of slides needed
   for slide_num in range(num_slides):
      # Create a new slide
      slide_layout = presentation.slide_layouts[6]  # Layout for a blank slide
      slide = presentation.slides.add_slide(slide_layout)

      # Get the image files for the current slide
      start_index = slide_num * images_per_slide
      end_index = start_index + images_per_slide
      current_image_files = [xs_image_files[slide_num],None,rate_image_files[slide_num],period_image_files[slide_num]]
      
      # Calculate the dimensions for each image placeholder
      image_width = Inches(4.5)
      image_height = Inches(3.75)
      left_positions = [Inches(0.25), Inches(5.)]
      top_positions = [Inches(0.25), Inches(4.)]
      
      # Iterate over the image files and add them to the slide
      counter = 0
      for left in left_positions:
         for top in top_positions:
               image_file = current_image_files[counter]
               counter += 1
               if image_file is None:
                  continue
               # Add an image placeholder to the slide
               slide.shapes.add_picture(image_file, left, top, width=image_width, height=image_height)
   
   slide = presentation.slides.add_slide(slide_layout)
   slide.shapes.add_picture(max_event_rate_file, Inches(0.25), Inches(0.25), width=Inches(9), height=Inches(7))

   num_slides = int(len(runtime_dist) / images_per_slide)
   # Iterate over the number of slides needed
   for slide_num in range(num_slides):
      # Create a new slide
      slide_layout = presentation.slide_layouts[6]  # Layout for a blank slide
      slide = presentation.slides.add_slide(slide_layout)

      # Get the image files for the current slide
      start_index = slide_num * images_per_slide
      end_index = start_index + images_per_slide
      current_image_files = [runtime_dist[slide_num*4+0],runtime_dist[slide_num*4+1],runtime_dist[slide_num*4+2],runtime_dist[slide_num*4+3]]
      
      # Calculate the dimensions for each image placeholder
      image_width = Inches(4.5)
      image_height = Inches(3.75)
      left_positions = [Inches(0.25), Inches(5.)]
      top_positions = [Inches(0.25), Inches(4.)]
      
      # Iterate over the image files and add them to the slide
      counter = 0
      for left in left_positions:
         for top in top_positions:
               image_file = current_image_files[counter]
               print(image_file)
               counter += 1
               if image_file is None:
                  continue
               # Add an image placeholder to the slide
               slide.shapes.add_picture(image_file, left, top, width=image_width, height=image_height)
   
   # Save the PowerPoint presentation
   presentation.save(output_file)

from pptx import Presentation
from pptx.util import Inches

def add_images_to_slide_2x2(prs, image_files):
    slide_layout = prs.slide_layouts[5]  # Use a blank slide layout
    slide = prs.slides.add_slide(slide_layout)
    
    # Assuming the slide size is standard with a 4:3 aspect ratio
    slide_width = prs.slide_width
    slide_height = prs.slide_height
    
    # Calculate image size (half the slide size, minus a small margin)
    img_width = slide_width / 2
    img_height = slide_height / 2
    
    positions = [(0, 0), (img_width, 0), (0, img_height), (img_width, img_height)]
    
    for idx, img_file in enumerate(image_files):
        left = positions[idx][0]
        top = positions[idx][1]
        slide.shapes.add_picture(img_file, left, top, width=img_width, height=img_height)

def add_images_to_slide_3x2(prs, image_files):
    slide_layout = prs.slide_layouts[5]  # Use a blank slide layout
    slide = prs.slides.add_slide(slide_layout)
    
    # Assuming the slide size is standard with a 4:3 aspect ratio
    slide_width = prs.slide_width
    slide_height = prs.slide_height
    
    # Calculate image size (one-third the slide width and half the slide height, minus a small margin)
    img_width = slide_width / 3
    img_height = slide_height / 2
    
    positions = [
        (0, 0), (img_width, 0), (2 * img_width, 0),
        (0, img_height), (img_width, img_height), (2 * img_width, img_height)
    ]
    
    for idx, img_file in enumerate(image_files):
        left = positions[idx][0]
        top = positions[idx][1]
        slide.shapes.add_picture(img_file, left, top, width=img_width, height=img_height)



def combined_datafiles(data_files,ref_file):
   dfs = []
   for data_file in data_files:
      df = pd.read_csv(data_file)
      df['impl'] = df['label']
      df['reference'] = False
      dfs.append(df)
   
   df = pd.read_csv(ref_file)
   df['impl'] = df['label'] + ' (CUDA)'
   df['reference'] = True
   dfs.append(df)

   combined_df = pd.concat(dfs).reset_index()

   return combined_df

def main():
   parser = argparse.ArgumentParser(description='Process and plot performance data.')
   parser.add_argument('-k','--kokkos-files', nargs='+',
                     help='CSV data files from kokkos runs on different architectures to be processed ')
   parser.add_argument('-c','--cuda-file', type=str,
                     help='CSV data file from cuda runs to be processed ')
   parser.add_argument('-o','--output-path', type=str,
                     help='path where to save plots',default='plots')

   args = parser.parse_args()

   # pd.set_option('display.max_columns', None)
   # pd.set_option('display.width', 2000)
   # pd.set_option('display.max_colwidth', -1)

   # Set up logging
   logging.basicConfig(filename=None, level=logging.INFO,
                     format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

   logging.info('Starting to process files: %s', ', '.join(args.kokkos_files))

   plt.rcParams.update({'font.size': 18})

   combined_df = combined_datafiles(args.kokkos_files,args.cuda_file)
   combined_df['efficiency'] = 0
   combined_df['cutEfficiency'] = 0
   combined_df['unweightingEfficiency'] = 0
   combined_df['cross_section'] = 0
   combined_df['cross_section_uncertainty'] = 0
   
   rows_with_nans = combined_df[combined_df.isna().any(axis=1)]

   logger.info("Removing these rows with NaNs:\n %s", rows_with_nans)
   combined_df = combined_df.dropna()


   kokkos_df = combined_df[combined_df['reference'] == False]

   if not os.path.exists(args.output_path):
      os.makedirs(args.output_path)

   try:
      plot_rate_period(combined_df,args.output_path)
      compare_implementations(combined_df)
      plot_cross_sections(combined_df,args.output_path,fn_extensions=['png','pdf'])
      # create_ppt_from_images(os.path.join(args.output_path,"pepper_runs_slides.pptx"))
      logging.info('Finished processing files')
   except Exception as e:
      logging.exception('Error processing files: %s', str(e))


if __name__ == '__main__':
   main()
