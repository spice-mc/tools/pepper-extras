#!/bin/bash
#PBS -l select=1
#PBS -l walltime=00:20:00
#PBS -A datascience
#PBS -q debug
#PBS -l filesystems=home:grand
#PBS -o logs/
#PBS -e logs/

NNODES=$(cat $PBS_NODEFILE | wc -l)
RANKS_PER_NODE=4
NRANKS=$(( $NNODES * $RANKS_PER_NODE ))
echo [$SECONDS] Running $NRANKS ranks on $NNODES nodes

MYPROJ_PATH=/lus/grand/projects/datascience/parton
KOKKOS_VERSION=3.7.02
ARCH=Kokkos_ARCH_AMPERE80
BUILD=Release
TEST_SCRIPT=~/pepper.ini
KOKKOS_PATH=/lus/grand/projects/datascience/parton/kokkos/kokkos-$KOKKOS_VERSION/$ARCH/$BUILD
PEPPER_PATH=/home/parton/git/pepper/build/kokkos-$KOKKOS_VERSION/$ARCH/$BUILD
source $KOKKOS_PATH/setup.sh
export PEPPER_DATA_PATH=/lus/grand/projects/datascience/parton/pepper_data
echo [$SECONDS] launching pepper
mpiexec -n $NRANKS -ppn $RANKS_PER_NODE --hostfile $PBS_NODEFILE $PEPPER_PATH/src/pepper $TEST_SCRIPT
echo [$SECONDS] done pepper
