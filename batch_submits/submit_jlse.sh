#!/bin/bash
#COBALT -n 1
#COBALT -t 360
#COBALT -A datascience

source /projects/datascience/parton/kokkos/kokkos-3.7.02/Kokkos_ARCH_SKX/Release/setup.sh
export PEPPER_DATA_PATH=/projects/AtlasESP/data/blockgen
export OMP_PROC_BIND=spread  OMP_PLACES=threads OMP_NUM_THREADS=56
echo [$SECONDS] launching pepper
/home/jchilders/git/pepper/build/kokkos-3.7.02/Kokkos_ARCH_SKX/Release/src/pepper ~/pepper.ini -g
echo [$SECONDS] done pepper
